package com.example.noteapp.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.noteapp.data.MyDb
import com.example.noteapp.data.UserDao
import com.example.noteapp.model.User

class UserRepo(private val userDao: UserDao) {

    var db: MyDb? = null
    var getUser: LiveData<User>? = null

    fun initDb(context: Context): MyDb {
        return MyDb.getDatabase(context)
    }

    suspend fun addUser(user: User) {
        userDao.addUser(user)
    }

    fun getUserLogin(context: Context, username: String, password:String): LiveData<User>? {
        db = initDb(context)
        getUser = db!!.userDao().getUserLogin(username,password)
        return getUser
    }

}