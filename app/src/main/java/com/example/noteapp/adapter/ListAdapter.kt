package com.example.noteapp.adapter

import android.app.AlertDialog
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Update
import com.example.noteapp.MainActivity
import com.example.noteapp.R
import com.example.noteapp.data.MyDb
import com.example.noteapp.fragments.HomeFragment
import com.example.noteapp.fragments.dialogs.AddDialogFragment
import com.example.noteapp.fragments.dialogs.UpdateDialogFragment
import com.example.noteapp.model.Note
import com.example.noteapp.viewmodel.NoteViewModel
import kotlinx.android.synthetic.main.item_note.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class ListAdapter: RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    private lateinit var mNoteViewModel: NoteViewModel

    private var noteList = emptyList<Note>()
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_note, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = noteList[position]
        holder.itemView.tvTitle.text = currentItem.title
        holder.itemView.tvBody.text = currentItem.body

        holder.itemView.ivDelete.setOnClickListener {
            val db = MyDb.getDatabase(holder.itemView.context)
            val dialog = AlertDialog.Builder(it.context)
            dialog.setTitle("Delete ${currentItem.title}")
            dialog.setMessage("Apakah anda yakin ingin menghapus note ${currentItem.title}?")
            dialog.setIcon(R.mipmap.ic_launcher)
            dialog.setCancelable(true)
            dialog.setPositiveButton("YES"){dialogInterface, p1 ->
                GlobalScope.async {
                    db.noteDao().deleteNote(currentItem)
                }
                Toast.makeText(it.context,"note ${currentItem.title} berhasil dihapus",Toast.LENGTH_LONG).show()
            }
            dialog.setNegativeButton("NO"){dialogInterface, p1 ->
                dialogInterface?.dismiss()
            }
            dialog.show()
        }

        holder.itemView.ivEdit.setOnClickListener{
            val activity = it.context as MainActivity
            val dialogFragment = UpdateDialogFragment(currentItem)
            dialogFragment.show(activity.supportFragmentManager, null)
        }
    }

    override fun getItemCount(): Int {
        return noteList.size
    }

    fun setData(note: List<Note>) {
        this.noteList = note
        notifyDataSetChanged()
    }
}