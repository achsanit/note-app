package com.example.noteapp.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.noteapp.R
import com.example.noteapp.databinding.FragmentLoginBinding
import com.example.noteapp.databinding.FragmentSplachScreenBinding
import com.example.noteapp.viewmodel.UserViewModel
import java.security.KeyStore

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private val sharedPrefFile = "shared_login"
    private lateinit var mUserVieModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.fragment_login, container, false)

        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mUserVieModel = ViewModelProvider(this).get(UserViewModel::class.java)
        binding.tvGoToRegister.setOnClickListener {
            val action = LoginFragmentDirections.actionLoginFragmentToRegisterFragment()
            findNavController().navigate(action)
        }

        binding.btnLogin.setOnClickListener {
            val username = binding.etUsername.editText?.text.toString()
            val password = binding.etPassword.editText?.text.toString()

            login(username,password)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun login(username: String, password: String) {
        if (inputCheck(username,password)){
            mUserVieModel.getUserLogin(requireContext(), username, password)?.observe(viewLifecycleOwner, Observer {
                if (it == null) {
                    Toast.makeText(context, "User tidak ditemukan!!", Toast.LENGTH_SHORT).show()
                } else {
                    val sharedPreferences: SharedPreferences = requireActivity().getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)

                    val editor: SharedPreferences.Editor = sharedPreferences.edit()
                    editor.putInt(KEY_ID, it.userId)
                    editor.putString(KEY_NAME, it.userName)
                    editor.putBoolean(IS_USER_LOGIN, true)
                    editor.apply()

                    Toast.makeText(context, "Berhasil Login...", Toast.LENGTH_SHORT).show()

                    //navigate to home
                    val action = LoginFragmentDirections.actionLoginFragmentToHomeFragment()
                    view?.findNavController()?.navigate(action,
                        NavOptions.Builder().setPopUpTo(R.id.loginFragment, true).build())
                }
            })
        } else {
            Toast.makeText(context, "Fields kosong", Toast.LENGTH_SHORT).show()
        }
    }

//    private fun login(username:String, password:String){
//        val sharedPreferences: SharedPreferences = requireActivity().getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
//        if (inputCheck(username,password)) {
//            val cekUser = mUserVieModel.getUserLogin(username,password)
//
//            if (cekUser) {
//                //use sharedpref for record the login
//                val editor: SharedPreferences.Editor = sharedPreferences.edit()
////                editor.putInt(KEY_ID, cekUser.userId)
////                editor.putString(KEY_NAME, username)
//                editor.putBoolean(IS_USER_LOGIN, true)
//                editor.apply()
//
//                //navigate to home
//                val action = LoginFragmentDirections.actionLoginFragmentToHomeFragment()
//                view?.findNavController()?.navigate(action,
//                    NavOptions.Builder().setPopUpTo(R.id.loginFragment, true).build())
//            } else {
//                Toast.makeText(requireContext(), "Failed Login", Toast.LENGTH_SHORT).show()
//            }
//        } else {
//            Toast.makeText(requireContext(), "Field masih kosong", Toast.LENGTH_SHORT).show()
//        }
//    }

    private fun inputCheck(username: String, password: String): Boolean {
        return !(TextUtils.isEmpty(username) || TextUtils.isEmpty(password))
    }

    companion object {
        val KEY_ID = "key_id"
        val KEY_NAME = "key_name"
        val IS_USER_LOGIN = "is_user_login"
    }
}