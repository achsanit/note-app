package com.example.noteapp.fragments.dialogs

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.noteapp.databinding.FragmentAddDialogBinding
import com.example.noteapp.fragments.LoginFragment
import com.example.noteapp.model.Note
import com.example.noteapp.viewmodel.NoteViewModel

class AddDialogFragment : DialogFragment() {
    private var _binding: FragmentAddDialogBinding? = null
    private val binding get() = _binding!!
    private val sharedPrefFile = "shared_login"
    private lateinit var mNoteViewModel: NoteViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_add_dialog, container, false)

        _binding = FragmentAddDialogBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mNoteViewModel = ViewModelProvider(this).get(NoteViewModel::class.java)

        binding.btnSave.setOnClickListener {
            addNote()
            dialog?.dismiss()
        }

        binding.btnCancle.setOnClickListener {
            dialog?.dismiss()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun addNote() {
        val sharedPreferences: SharedPreferences = requireActivity().getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        val userId = sharedPreferences.getInt(LoginFragment.KEY_ID,0)
        val title = binding.etTitle.editText?.text.toString()
        val body = binding.etBody.editText?.text.toString()

        if (inputCheck(title,body)) {
            val newNote = Note(0,userId,title,body)
            mNoteViewModel.addNote(newNote)
            Toast.makeText(requireContext(), "note baru tersimpan", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Fields kosong", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputCheck(title: String, body:String): Boolean {
        return !(TextUtils.isEmpty(title) || TextUtils.isEmpty(body))
    }
}