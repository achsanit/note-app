package com.example.noteapp.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import com.example.noteapp.R
import com.example.noteapp.databinding.FragmentSplachScreenBinding

class SplachScreenFragment : Fragment() {
    private var _binding: FragmentSplachScreenBinding? = null
    private val binding get() = _binding!!
    private val sharedPrefFile = "shared_login"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.fragment_splach_screen, container, false)

        _binding = FragmentSplachScreenBinding.inflate(inflater, container, false)
        val view = binding.root
        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()

        val sharedPreferences: SharedPreferences = activity?.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)!!

        if (sharedPreferences.contains(LoginFragment.IS_USER_LOGIN)) {
            Handler(Looper.getMainLooper()).postDelayed({
                val action = SplachScreenFragmentDirections.actionSplachScreenFragmentToHomeFragment2()
                view.findNavController().navigate(action,
                    NavOptions.Builder().setPopUpTo(R.id.splachScreenFragment, true).build())
            },3000)
        } else {
            Handler(Looper.getMainLooper()).postDelayed({
                val action = SplachScreenFragmentDirections.actionSplachScreenFragmentToLoginFragment()
                view.findNavController().navigate(action,
                    NavOptions.Builder().setPopUpTo(R.id.splachScreenFragment, true).build())
            },3000)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}