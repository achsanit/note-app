package com.example.noteapp.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.noteapp.R
import com.example.noteapp.adapter.ListAdapter
import com.example.noteapp.databinding.FragmentAddDialogBinding
import com.example.noteapp.databinding.FragmentHomeBinding
import com.example.noteapp.databinding.FragmentRegisterBinding
import com.example.noteapp.fragments.dialogs.AddDialogFragment
import com.example.noteapp.viewmodel.NoteViewModel
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val sharedPrefFile = "shared_login"
    private lateinit var mNoteViewModel: NoteViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_home, container, false)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = ListAdapter()
        val recycler = view.recyclerView
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(requireContext())

        mNoteViewModel = ViewModelProvider(this).get(NoteViewModel::class.java)
        mNoteViewModel.getNote.observe(viewLifecycleOwner, {adapter.setData(it)})

        val sharedPreferences: SharedPreferences = activity?.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)!!
        val nameValue = sharedPreferences.getString(LoginFragment.KEY_NAME, "kosong")

        binding.tvTess.text = "Welcome, ${nameValue}!"

        binding.floatingActionButton.setOnClickListener {
            val dialogFragment = AddDialogFragment()
            dialogFragment.show(childFragmentManager,null)
        }

        binding.tvLogout.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Logout")
            dialog.setMessage("Apakah anda yakin ingin logout dari aplikasi?")

            dialog.setCancelable(true)
            dialog.setPositiveButton("YES"){dialogInterface, p1 ->
                Toast.makeText(requireContext(),"Berhasil logout...",Toast.LENGTH_LONG).show()
                logout()
            }

            dialog.setNegativeButton("NO"){dialogInterface, p1->
                dialogInterface.dismiss()
            }

            dialog.show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun logout() {
        val sharedPreferences: SharedPreferences = activity?.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)!!
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()

        val action = HomeFragmentDirections.actionHomeFragmentToLoginFragment()
        view?.findNavController()?.navigate(action,
            NavOptions.Builder().setPopUpTo(R.id.homeFragment, true).build())
    }

}