package com.example.noteapp.fragments

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import com.example.noteapp.R
import com.example.noteapp.databinding.FragmentLoginBinding
import com.example.noteapp.databinding.FragmentRegisterBinding
import com.example.noteapp.model.User
import com.example.noteapp.viewmodel.UserViewModel

class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!
    private lateinit var mUserVieModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_register, container, false)
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mUserVieModel = ViewModelProvider(this).get(UserViewModel::class.java)

        binding.tvGoToLogin.setOnClickListener {
            val action = RegisterFragmentDirections.actionRegisterFragmentToLoginFragment()
            view.findNavController().navigate(action,
                NavOptions.Builder().setPopUpTo(R.id.registerFragment, true).build())
        }

        binding.btnRegister.setOnClickListener {
            register()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun register(){
        val userName = binding.etUsername.editText?.text.toString()
        val email = binding.etEmail.editText?.text.toString()
        val password = binding.etPassword.editText?.text.toString()
        val confirmPass = binding.etConfirmPassword.editText?.text.toString()

        if (inputCheck(userName, email, password, confirmPass)) {
            if (password == confirmPass) {
                val newUser = User(0, userName, email, password)

                mUserVieModel.addUser(newUser)
                Toast.makeText(requireContext(), "Selamat registrasi berhasil!", Toast.LENGTH_SHORT).show()

                val action = RegisterFragmentDirections.actionRegisterFragmentToLoginFragment()
                view?.findNavController()?.navigate(action,
                    NavOptions.Builder().setPopUpTo(R.id.registerFragment, true).build())

            } else {
                Toast.makeText(requireContext(), "Konfirmasi password salah!!", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(requireContext(), "lengkapi isi fields!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputCheck(username: String, email:String, password: String, confirmPass: String): Boolean {
        return !(TextUtils.isEmpty(username) || TextUtils.isEmpty(email) ||
                TextUtils.isEmpty(password) || TextUtils.isEmpty(confirmPass))
    }
}