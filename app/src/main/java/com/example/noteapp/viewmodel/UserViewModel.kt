package com.example.noteapp.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.noteapp.data.MyDb
import com.example.noteapp.model.User
import com.example.noteapp.repository.UserRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel(application: Application): AndroidViewModel(application) {

    private var getUser: LiveData<User>?
    private val userRepo: UserRepo

    init {
        val userDao = MyDb.getDatabase(application).userDao()
        userRepo = UserRepo(userDao)
        getUser = userRepo.getUser
    }

    fun addUser(user: User) {
        viewModelScope.launch(Dispatchers.IO) {
            userRepo.addUser(user)
        }
    }

    fun getUserLogin(context: Context, username: String, password:String): LiveData<User>? {
        getUser = userRepo.getUserLogin(context, username, password)
        return getUser
    }
}