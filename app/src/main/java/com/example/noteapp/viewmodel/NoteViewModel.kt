package com.example.noteapp.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.noteapp.data.MyDb
import com.example.noteapp.model.Note
import com.example.noteapp.repository.NoteRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NoteViewModel(application: Application): AndroidViewModel(application) {

    val getNote: LiveData<List<Note>>
    private val noteRepo: NoteRepo

    init {
        val noteDao = MyDb.getDatabase(application).noteDao()
        noteRepo = NoteRepo(noteDao)
        getNote = noteRepo.getNote
    }

    fun addNote(note:Note) {
        viewModelScope.launch(Dispatchers.IO) {
            noteRepo.addNote(note)
        }
    }

    fun deleteNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            noteRepo.deleteNOte(note)
        }
    }

    fun updateNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            noteRepo.updateNote(note)
        }
    }
}