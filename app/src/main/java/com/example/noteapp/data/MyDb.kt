package com.example.noteapp.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.noteapp.model.Note
import com.example.noteapp.model.User

@Database(entities = [User::class, Note::class], version = 2, exportSchema = false)
abstract class MyDb: RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun noteDao(): NoteDao

    companion object {
        private var INSTANCE: MyDb? = null

        fun getDatabase(context: Context): MyDb {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(MyDb::class.java) {
                val instance = Room.databaseBuilder(
                    context.applicationContext, MyDb::class.java,
                    "my_db"
                ).fallbackToDestructiveMigration().build()
                INSTANCE = instance
                return instance
            }
        }
    }

}